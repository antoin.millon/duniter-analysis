import igraph
import glob
import numpy as np

#create a npy file that represents the evolution of the diameter each day_interval days
#returns the number of graphs(s) processed
def diameter_analysis(pickle_directory, analysis_data_directory, day_interval, directed = False, debug = False):
    if debug:
        print("[STARTING] diameter analysis")
    #Getting all pickle filenames
    all_pickle_filenames = glob.glob(pickle_directory + "*.pickle")
    #Initializing output array
    tab_diameter = np.array([])

    #We analyse each day interval
    for i in range(0, len(all_pickle_filenames), day_interval):
        pickle_filename = all_pickle_filenames[i]
        if debug:
            print("[INFO] Processing diameter on ", pickle_filename)

        #Converting file into graph object
        g = igraph.Graph.Read_Pickle(pickle_filename)


        #Adding the new diameter value
        tab_diameter = np.concatenate([tab_diameter, [g.diameter(directed = directed)]])

    analysis_file = analysis_data_directory + "diameter" + str(day_interval) + int(directed == True)*"DIRECTED" + ".npy"
    #Saving the tab_diameter object into a file
    np.save(analysis_file, tab_diameter)
    if debug:
        print("[INFO] Created {}".format(analysis_file))
        print("[FINISHED] diameter analysis")
    return len(tab_diameter)


#create a npy file that represents the evolution of average path length
#returns the number of pickle file(s) processed
def avg_path_length_analysis(pickle_directory, analysis_directory, day_interval, directed = False, debug = False):
    if debug:
        print("[STARTING] average path length analysis")
    #Getting all pickle filenames
    all_pickle_filenames = glob.glob(pickle_directory + "*.pickle")
    #Initializing output array
    tab_avg_path_length = np.array([])

    #We analyse each day interval
    for i in range(0, len(all_pickle_filenames), day_interval):
        pickle_filename = all_pickle_filenames[i]

        if debug:
            print("[INFO] Processing avg_path_length on ", pickle_filename)

        #Converting file into graph object
        g = igraph.Graph.Read_Pickle(pickle_filename)

        #Adding the new diameter value
        tab_avg_path_length = np.concatenate([tab_avg_path_length, [g.average_path_length(directed = directed)]])
    
    analysis_file = analysis_directory + "avg_path_length" + str(day_interval) + int(directed == True)*"DIRECTED" + ".npy"

    #Saving the tab_diameter object into a file
    np.save(analysis_file, tab_avg_path_length)
    if debug:
        print("[INFO] Created {}".format(analysis_file))
        print("[FINISHED] average path length analysis")
    return len(tab_avg_path_length)


#create a npy file that represents the evolution of the closeness
#returns the number of pickle file(s) processed
def closeness_analysis(pickle_directory, analysis_directory, output_filename, day_interval, mode = "all", debug = False):
    if debug:
        print("[STARTING] closeness analysis")
    from itertools import zip_longest
    #Getting all pickle filenames
    all_pickle_filenames = glob.glob(pickle_directory + "*.pickle")
    #Initializing output matrix
    matrix_closeness = []

    #We analyse each day interval
    for i in range(0, len(all_pickle_filenames), day_interval):
        pickle_filename = all_pickle_filenames[i]
        if debug:
            print("[INFO] Processing closeness on ", pickle_filename)

        #Converting file into graph object
        g = igraph.Graph.Read_Pickle(pickle_filename)

        #Adding the new diameter value
        matrix_closeness.append(g.closeness(mode = mode))

    #Padding with zeros, in fact the number of vertices depends of the time period of the graph
    # So we say that the values of closeness for not yet created vertices are 0 
    matrix_closeness = np.array(list(zip_longest(*matrix_closeness, fillvalue=0))).T

    suffixe = int(mode == "in")*"IN" + int(mode == "out")*"OUT"
    analysis_file = analysis_directory + "avg_path_length" + str(day_interval) + suffixe + ".npy"

    #Saving the tab_diameter object into a file
    np.save(output_filename, matrix_closeness)
    if debug:
        print("[INFO] Created {}".format(analysis_file))
        print("[FINISHED] closeness analysis")
    return len(matrix_closeness)
