import igraph
import matplotlib.pyplot as plt



# nb_vertices: Number of vertices that one want to have in the random generated graph
# dist: distance for which an edge will be drawn between two vertices
def random_graph(nb_vertices, dist, layout):

    g = igraph.Graph.GRG(nb_vertices, dist)

    fig, ax = plt.subplots()
    igraph.plot(g, layout=g.layout(layout), target = ax)
    return g