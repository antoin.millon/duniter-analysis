import matplotlib.pyplot as plt
import numpy as np
from matplotlib import cm
import glob

#Creates a png file representing the plot
def diameter_plot(pickle_directory, analysis_data_directory, output_filename, day_interval, directed = False, debug = False):
    if debug:
        print("[STARTING] Creating a diameter plot for each {} day(s)".format(day_interval))

    analysis_file = analysis_data_directory + "diameter" + str(day_interval) + int(directed == True)*"DIRECTED" + ".npy"
    diameter_file = glob.glob(analysis_file)

    if diameter_file == []:
        from WoT_analysis import diameter_analysis
        diameter_analysis(pickle_directory, analysis_data_directory, day_interval, directed = directed, debug = debug)

    tab_diameter = np.load(analysis_file)

    fig, ax = plt.subplots()
    ax.plot(np.arange(1,len(tab_diameter)+1)*day_interval, tab_diameter)
    ax.set_ylabel("Diameter")
    ax.set_xlabel("Days")
    fig.savefig(output_filename)
    if debug:
        print("[INFO] Created {}.".format(output_filename))
        print("[FINISHED]")

#Creates a png file representing the plot
def avg_path_length_plot(pickle_directory, analysis_data_directory, output_filename, day_interval, directed = False, debug = False):
    if debug:
        print("[STARTING] Creating an average path length plot for each {} days of interval".format(day_interval))
    analysis_file = analysis_data_directory + "avg_path_length" + str(day_interval) + int(directed == True)*"DIRECTED" + ".npy"
    avg_path_length_file = glob.glob(analysis_file)

    if avg_path_length_file == []:
        from WoT_analysis import avg_path_length_analysis
        avg_path_length_analysis(pickle_directory, analysis_data_directory, day_interval, directed = directed, debug = debug)

    tab_avg_path_length = np.load(analysis_file)

    fig, ax = plt.subplots()
    ax.plot(np.arange(1,len(tab_avg_path_length)+1)*day_interval, tab_avg_path_length)
    ax.set_ylabel("average path length")
    ax.set_xlabel("Days")
    fig.savefig(output_filename)
    if debug:
        print("[INFO] Created {}".format(output_filename))
        print("[FINISHED]")

#Creates a png file representing the plot
def closeness_plot(pickle_directory, analysis_data_directory, output_filename, day_interval, mode = "all", debug = False):
    if debug:
        print("[STARTING] Creating a closeness plot for each {} days of interval".format(day_interval))

    suffixe = int(mode == "in")*"IN" + int(mode == "out")*"OUT"
    analysis_file = analysis_data_directory + "avg_path_length" + str(day_interval) + suffixe + ".npy"
    closeness_file = glob.glob(analysis_file)

    if closeness_file == []:
        from WoT_analysis import closeness_analysis
        closeness_analysis(pickle_directory, analysis_data_directory, output_filename, day_interval, mode = mode, debug = debug)

    matrix_closeness = np.load(analysis_file)

    fig, ax = plt.subplots(subplot_kw={"projection": "3d"})


    sommets = np.arange(1, len(matrix_closeness[0]) + 1) #Les differents sommets
    #sommets = np.arange(1, 20)
    jours = np.arange(1, len(matrix_closeness)+1)*day_interval #Les differents jours


    SOMMETS, JOURS = np.meshgrid(sommets, jours)

    CLOSENESS = np.array(matrix_closeness)


    surf = ax.plot_surface(SOMMETS, JOURS, CLOSENESS[:,:10], cmap=cm.coolwarm)

    plt.savefig(output_filename)
    if debug:
        print("[INFO] Created {}".format(output_filename))
        print("[FINISHED]")



