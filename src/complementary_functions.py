import glob

#Downloads lg files
#return the number of lg file(s) downloaded
def http_update_lg_files(lg_directory, debug = False):
    import requests
    session = requests.Session()
    base_url = "https://files.datajune.coinduf.eu/graphs.lg/"

    #--------Here are looking for the number of the current last lg file in our directory---------
    all_lg_filenames = glob.glob(lg_directory + "*.lg")

    last_file_number = "0001" 

    for file_name in all_lg_filenames:
        actual_file_number = file_name.split(".")[0][-4:]
        if int(actual_file_number) > int(last_file_number):
            last_file_number = actual_file_number
    #--------------------------------------------------------------------

    #We add one to the current last file number, for the next file to download
    last_file_number = "0"*(4-len(str(int(last_file_number) + 1))) + str(int(last_file_number) + 1)

    if debug:
        print("[STARTING] Connection to Datajune server : https://files.datajune.coinduf.eu")
    nb = 0
    #Here we are getting all files that we do not have
    while True:
        url = base_url + last_file_number + ".lg"
        r = session.get(url)
        if r.status_code != 200:
            break
        if debug:
            print("[STARTING] Downloaded {}.lg".format(last_file_number))

        #Writting content of the dowlloadded lg file in our lg directory
        open(lg_directory + last_file_number + ".lg", 'wb').write(r.content)

        #we add 1 to filename
        last_file_number = "0"*(4-len(str(int(last_file_number) + 1))) + str(int(last_file_number) + 1)
        nb += 1
    if debug:
        if nb == 0:
            print("[INFO] Everything up to date!")
        else:
            print("[INFO] Downloaded {} files".format(nb))
        print("[FINISHED] Connection terminated")
    return nb


#return number of lg file(s) converted
def lg_to_pickle(lg_directory, pickle_directory, debug = False):
    if debug:
        print("[STARTING] Converting lg files to pickle: LG => PICKLE")
    import igraph
    all_lg_filenames = glob.glob(lg_directory + "*.lg")
    pickle_files = glob.glob(pickle_directory + "*.pickle")
    nb = 0
    #for lg_filename in all_lg_filenames[len(pickle_files):]:
    for lg_filename in all_lg_filenames:

        pickle_filename = pickle_directory + lg_filename.split("/")[-1][:-3] + ".pickle"
        if pickle_filename in pickle_files:
            continue
        if debug:
            print("[INFO] Converting " + lg_filename)
        g = lg_reader(lg_filename)
        g.write_pickle(pickle_filename)
        nb += 1
    if debug:
        print("[FINISHED] All lg files has been converted into a pickle format: LG => PICKLE")
    return nb



#Transform lg file format into pickle graph format
def lg_reader(lg_filename):
    import igraph
    g = igraph.Graph(directed = True)

    with open(lg_filename, 'r') as file:
        first_line = file.readline()
        g.add_vertices(int(first_line.split(",")[0]))

        for other_line in file:
            tmp = other_line.split(",")
            g.add_edge(int(tmp[0])-1, int(tmp[1][:-1])-1)
    return g

def npy_update(pickle_directory, npy_filename, day_interval, directed = False, debug = False):
    import numpy as np
    import igraph
    npy_tab = np.load(npy_filename)
    all_pickle_filenames = glob.glob(pickle_directory + "*.pickle")
    if debug:
        print("[STARTING] Updating ", npy_filename)

    if len(npy_tab) < len(all_pickle_filenames):
        #We analyse each day interval
        for i in range(len(npy_tab), len(all_pickle_filenames), day_interval):
            pickle_filename = all_pickle_filenames[i]
            if debug:
                print("[INFO] Processing diameter on ", pickle_filename)

            #Converting file into graph object
            g = igraph.Graph.Read_Pickle(pickle_filename)


            #Adding the new diameter value
            npy_tab = np.concatenate([npy_tab, [g.diameter(directed = directed)]])

        np.save(npy_filename, npy_tab)
    if debug:
        print("[FINISHED] Updating ", npy_filename)



#list is a list of days that we are seeking for
def get_pickles_filenames(pickle_directory, list = []):
    import glob
    all_pickle_filenames = glob.glob(pickle_directory + "*.pickle")
    if list == []:

        return all_pickle_filenames
    else:
        l = []
        for num in list:
            l.append(pickle_directory + "0"*(4-len(str(num))) + str(num) + ".pickle")
        return l