#!/usr/bin/env python3


#list is a list of days that we are seeking for
def get_pickles_filenames(pickle_directory, list = []):
    import glob
    all_pickle_filenames = glob.glob(pickle_directory + "*.pickle")
    if list.is_empty():

        return all_pickle_filenames
    l = []
    for num in list:
        l.append(pickle_directory + str(num))
    for i in range(0, len(all_pickle_filenames), 60):
        pickle_filename = all_pickle_filenames[i]
        print("Processing ", pickle_filename)
        graph_filename = graph_directory + pickle_filename.split("/")[-1][:-7] + ".png"

        g = igraph.Graph.Read_Pickle(pickle_filename)

        for i in range(g.vcount()):
            g.vs[i]['color'] = colors[i]

        #Directed recursive algorithm to visualize the vertex and edges
        layout = g.layout_graphopt()

        igraph.plot(g, graph_filename, layout=layout, \
                    edge_arrow_size = 0.3, edge_arrow_width = 0.5,\
                    vertex_size = 6, edge_width = 1, bbox=(800,800),\
                    autocurve = True)
        #g.diameter()
