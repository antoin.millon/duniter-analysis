#!/usr/bin/env python3

#Scripts that is used for analysis purposes

import sys
sys.path.append('/home/cyanure/STAGE/duniter-analysis/src')
from WoT_analysis_boxplot import diameter_boxplot
import numpy as np
import igraph
import glob

#--------DIRECTORIES NAME------
pickle_directory = "/home/cyanure/STAGE/duniter-analysis/pickle_files/"
analysis_directory = "/home/cyanure/STAGE/duniter-analysis/analysis/"
analysis_data_directory = analysis_directory + "data/"
graph_directory = "/home/cyanure/STAGE/duniter-analysis/graph_images/"
lg_directory = "/home/cyanure/STAGE/duniter-analysis/lg_files/"
#-------------------------------


#-------------Time step---------
day_interval = 1
#-------------------------------


diameter_undirected = analysis_directory + "plots/" + "boxplot_diameter1undirected"
diameter_directed = analysis_directory + "plots/" + "boxplot_diameter1directed"


analysis_data_directory = analysis_directory + "data/"
day_interval = 1


diameter_boxplot(pickle_directory, analysis_data_directory, diameter_directed, day_interval, directed = True, debug = True)
diameter_boxplot(pickle_directory, analysis_data_directory, diameter_undirected, day_interval, directed = False, debug = True)
