#!/usr/bin/env python3

#Scripts that is used for analysis purposes

import sys
sys.path.append('/home/cyanure/STAGE/duniter-analysis/src')

from complementary_functions import npy_update
import numpy as np



#--------DIRECTORIES NAME------
pickle_directory = "/home/cyanure/STAGE/duniter-analysis/pickle_files/"
analysis_directory = "/home/cyanure/STAGE/duniter-analysis/analysis/"
analysis_data_directory = analysis_directory + "data/"
graph_directory = "/home/cyanure/STAGE/duniter-analysis/graph_images/"
lg_directory = "/home/cyanure/STAGE/duniter-analysis/lg_files/"
#-------------------------------



npy_update(pickle_directory, analysis_data_directory + "diameter1.npy", 1, directed = False, debug = True)
npy_update(pickle_directory, analysis_data_directory + "diameter1DIRECTED.npy", 1, directed = False, debug = True)
npy_update(pickle_directory, analysis_data_directory + "avg_path_length1DIRECTED.npy", 1, directed = False, debug = True)
npy_update(pickle_directory, analysis_data_directory + "avg_path_length1.npy", 1, directed = False, debug = True)