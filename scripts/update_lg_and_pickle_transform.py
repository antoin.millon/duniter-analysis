#!/usr/bin/env python3

#Script that is used to synchronise the evolution of the G1 Web with the DataJune server


import sys
sys.path.append('/home/cyanure/STAGE/duniter-analysis/src')
from complementary_functions import http_update_lg_files, lg_to_pickle



#--------DIRECTORIES NAME------
pickle_directory = "pickle_files/"
lg_directory = "lg_files/"
#-------------------------------



http_update_lg_files(lg_directory, debug = True)
lg_to_pickle(lg_directory, pickle_directory, debug = True)



