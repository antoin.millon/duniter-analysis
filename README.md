Tout les resultat presenté dans ce depot sont effectué a partirt des differents fichiers *.lg de cette URL https://files.datajune.coinduf.eu/graphs.lg/. Ces fichiers ont eté téléchargé dans le repertoire lg_files/.

On a utilisé la librairie igraph de python pour l'étude de graphe. Celle-ci charge beaucoup plus rapidement un fichier sous format .pickle, c'est pourquoi on a transformé tout les fichiers du format .lg en format .pickle avant de les utiliser.

Le fichier complementary_functions.py est un ensemble de fonctions utile pour les fichiers ci dessous. Notamment ce fichier implémente l'automatisation du telechargement de nouveau fichier .lg disponible sur Datajune.

Le fichier WoT_analysis.py contient un ensemble de fonction permettant d'analyser un ensemble de graphes. L'effet de bord est la création de fichier .npy. Notamment, il est necessaire de préciser :
- le repèrtoire contenant les fichiers .pickle a analyser
- le repertoire ou vont etre creer le fichier d'analyse
- le nombre de jours d'intervalle de l'étude

Le fichier WoT_analysis_plot.py regroupe un ensemble de fonction qui non seulement crée un fichier d'analyse (comme les fonctions du fichier WoT_analysis.py) si le fichier n'est pas déja présent mais qui crée une image png du graphique représentant les donnees d'analyses voulu. De maniere semblable, on doit fournir :

- le repèrtoire contenant les fichiers .pickle a analyser
- le repertoire contenant les fichiers d'analyse (output des fonctions contenu dans WoT_analysis.py)
- le nombre de jours d'intervalle de l'étude

Le script script.py est un exemple d'utilisation de nos fonctions/fichiers, il crée un graphique de l'évolution du diamétre des graphes en fonction du temps (les graphes sont stocké sous la forme de .pickle dans le répertoire pickle_files/). Le graphe diametre a une erreur relative a ons donnees.







